# How to Migrate and Use Salt Files from Artifactory #

If your organization has a large Salt repo managed by several users, and/or utilizes several similar files or packages scattered throughout different subfolders (e.g. different versions of Java JDK are needed on different servers), you should consider migrating these files to a repository manager like Artifactory.

<br />

#### Why Do This? ####

1. Migrating the large files to a repository manager will both reduce the size of your Salt repo and increase its performance
2. Consolidating your similar files and packages will make it significantly easier to find and manage these files

<br />

#### Before You Begin ####

To perform the steps in this tutorial, you will need the following:

- A fully-configured Salt server running on Linux with a CLI (CentOS/RedHat, Ubuntu, etc.) managed in a <em>git</em> repo
- An instance of Artifactory running with <a href="https://jfrog.com/knowledge-base/what-ports-should-i-expose-when-setting-up-jfrog-applications/">THESE PORTS</a> accessible to your Salt server

<br /><br />


<!------------>
<!-- STEP 1 -->
<!------------>
## 1. Identify Desired Files To Move from Your Salt Repo to Artifactory by Size or Type ##

Before migrating any files to Artifactory, you will need to identify them in Salt. To reap the most benefits of migrating files to Artifactory, you will want migrate the largest files and/or all files of a particular type.

<br />

### Identify Largest Files in Salt by Size ###

If reducing the size of your Salt repo is your main concern, you can use commands like these below to identify the largest files in the Salt repo. This will allow you to migrate the largest files first and speed up your repo's performance as quickly as possible.

Simply input the path to your Salt folder into the <code>salt_folder</code> variable below, then run the command:

<pre><code># Defines the local path to the Salt folder #
salt_folder_path="/location-of-salt-folder"

# Shows first 10 largest files - adjust the number as needed #
find $salt_folder_path -type f -exec ls -sh {} + | sort -n -r | head -10
</code></pre>

<br />

### Identify Specific File Types in Salt ###

You can also be more specific. For example, if you decided to store and manage all **.jar** files in Artifactory, you could run commands like these below to identify all of the files of that type that would need to be moved.

Simply replace the <code>file_type</code> and <code>salt_folder</code> variables with the relevant values:

<pre><code># Defines the desired file type to search for #
file_type=".jar"

# Defines the local path to the Salt folder #
salt_folder_path="/location-of-salt-folder"

# Searches for all files of specified type in Salt folder and sorts by size in descending order (largest to smallest) #
find $salt_folder_path -type f -exec ls -sh {} + | sort -n -r | grep $file_type

# Same command as above, but only shows first 10 files - adjust the number as needed #
find $salt_folder_path -type f -exec ls -sh {} + | sort -n -r | grep $file_type | head -10
</code></pre>

<br /><br />


<!------------>
<!-- STEP 2 -->
<!------------>
## 2. Migrate Files from Salt to Artifactory ## 

Once you have identified the file(s) to migrate to and manage in Artifactory, you can upload them with the following steps:

1. Open a browser, navigate to your Artifactory repo, and log in<br /><br />
2. Click **Artifacts** on the left navigation bar<br /><br />
3. Navigate to the folder you wish to upload the files to, click on and highlight the folder, then click the **Deploy** button in the top right<br /><br />
4. In the window that pops up, specify the desired full path and filename under **Target Path**, then click **Deploy** - For example: <code>/folder/file.jar</code>
    * <span style="color:red">NOTE:</span> if you are creating a new folder or subfolder to store the file in, include the new folder in the path: <code>/folder/new-subfolder/file.jar</code><br /><br />
5. When deployment is finished, make note of the **URL to file** and **SHA-256** values<br /><br />

<br />

<!------------>
<!-- STEP 3 -->
<!------------>
## 3. Replace the Original Salt State References ##
Now that the file has been migrated to Artifactory, the sources that Salt references in the **.sls** files to utilize the file must be updated.

<br />

### 3a. Identify Salt Files that Reference the Migrated File(s) ###
The command below searches every folder, subfolder, and file in the Salt folder for all **.sls** files that reference the original file name that was migrated.

<pre><code># Stores original file name as variable # 
original_file_name="file.jar"

# Defines the local path to the Salt folder #
salt_folder_path="/location-of-salt-folder"

grep -rwl $original_file_name $salt_folder_path/* | grep .sls
</code></pre>

<br />

### 3b. Replace the "source" Reference in the .sls Files ###

Now ALL references to the original file in Salt states must be updated with the URL from Artifactory.

#### Original File Reference: ####
The original reference to the file when it existed in Salt will look something like this:

<pre><code>file_example:
  file.managed:
    - source: salt://path-to-file-in-salt/file.jar
    - user: some_user
    - group: some_group
</code></pre>

#### New File Reference: ####
To update the reference so it pulls this file from Artifactory instead of Salt, you must: 
1. Replace the value of <code>source</code> with the **URL to file** value from Artifactory in the last step 
2. Add a new parameter called <code>source_hash</code> set to <code>sha256=</code> and the **SHA-256** value from the last step (shown as <code>#</code> symbols below)

<pre><code>file_example:
  file.managed:
    - source: http://artifactory-url/artifactory/path-to-file/
    - source_hash: sha256=################################################################
    - user: some_user
    - group: some_group
</code></pre>

<br />

### 3c. Delete The Original File from Salt ###
Once the file is in Artifactory and the source reference has been updated, the original file can be deleted from Salt using a command similar to the one below:

<pre><code>rm -rf /location-of-salt-folder/path-to-file-in-salt/file.jar</code></pre>

<br /><br />

## 4. Update the Repo ## 

Lastly, push your changes to your Salt git repo.

<pre><code>git commit -am "Updated source references of {file.jar} after migrating it to Artifactory"
git push</code></pre>

<br /><br />

## 5. Upkeep ##

Make sure that you advise your Salt users to store designated files in Artifactory and reference them correctly going forward. No one likes technical debt! :)
